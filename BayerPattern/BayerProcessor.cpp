#include <cassert>
#include <cmath>
#include <exception>
#include "BayerProcessor.h"

CBayerProcessor::CBayerProcessor( Gdiplus::BitmapData& imagedata ) :
	image( imagedata ), width( imagedata.Width ), height( imagedata.Height )
{
	if( width < 3 || height < 3 ) {
		assert( false );
		throw new std::logic_error( "Small image" );
	}

	red.reserve( width * height );
	green.reserve( width * height );
	blue.reserve( width * height );

	const int bytesPerRow = imagedata.Stride;
	const int bytesPerPixel = 3; // BGR24
	unsigned char* buffer = (unsigned char*)imagedata.Scan0;
	int baseAdr = 0;
	for( int y = 0; y < height; y++ ) {
		int pixelAdr = baseAdr;
		for( int x = 0; x < width; x++ ) {
			register unsigned char b = buffer[pixelAdr]; // blue
			blue.push_back( b );
			register unsigned char g = buffer[pixelAdr + 1]; // green
			green.push_back( g );
			register unsigned char r = buffer[pixelAdr + 2]; // red
			red.push_back( r );

#if _DEBUG
			if( x % 2 == 1 && y % 2 == 1 ) {
				assert( g == 0 && r == 0 );
			}
			if( x % 2 == 0 && y % 2 == 0 ) {
				assert( b == 0 && g == 0 );
			}
			if( (x + y) % 2 == 1 ) {
				assert( r == 0 && b == 0 );
			}
#endif
			pixelAdr += bytesPerPixel;
		}
		baseAdr += bytesPerRow;
	}
}

CBayerProcessor::~CBayerProcessor()
{}

const __int16 maxByte = (1 << 8) - 1;

inline static unsigned int fitByte( __int16 value )
{
	return min( max( value, 0 ), maxByte );
}

inline static unsigned int hueTransit( int l1, int l2, int l3, int v1, int v3 )
{
	register int result;
	if( (l1 < l2 && l2 < l3) || (l1 > l2 && l2 > l3) ) {
		result = v1 + (v3 - v1) * (l2 - l1) / (l3 - l1);
	} else {
		result = (v1 + v3) / 2 + (l2 - (l1 + l3) / 2) / 2;
	}

	return fitByte( result );
}

// ������ ��������, ����������� ���� ������
inline static __int16 absDiff( unsigned int a, unsigned int b )
{
	return abs( (static_cast<__int16>(a)) - (static_cast<__int16>(b)) );
}

inline static void updateGradient( int dx, int dy, __int16 value, int& argDx, int& argDy, __int16& minValue )
{
	if( value < minValue ) {
		argDx = dx;
		argDy = dy;
		minValue = value;
	}
}


inline __int16 CBayerProcessor::redGradient( int x, int y, int dx, int dy )
{
	assert( abs( dx + dy ) == 1 );
	assert( abs( dx ) <= 1 );
	assert( abs( dy ) <= 1 );

	return absDiff( getOriginalR( x, y ), getOriginalR( x + 2 * dx, y + 2 * dy ) ) * 2 + absDiff( getOriginalG( x + dx, y + dy ), getOriginalG( x - dx, y - dy ) );
}

inline __int16 CBayerProcessor::blueGradient( int x, int y, int dx, int dy )
{
	assert( abs( dx + dy ) == 1 );
	assert( abs( dx ) <= 1 );
	assert( abs( dy ) <= 1 );

	return absDiff( getOriginalB( x, y ), getOriginalB( x + 2 * dx, y + 2 * dy ) ) * 2 + absDiff( getOriginalG( x + dx, y + dy ), getOriginalG( x - dx, y - dy ) );
}

void CBayerProcessor::greenByGradientMinRed()
{
	for( int x = 2; x < width - 2; x += 2 ) {
		for( int y = 2; y < height - 2; y += 2 ) {
			int dx = 0;
			int dy = 0;
			__int16 minGradient = (1 << 15) - 1;
			updateGradient( -1, 0, redGradient( x, y, -1, 0 ), dx, dy, minGradient );
			updateGradient( 1, 0, redGradient( x, y, 1, 0 ), dx, dy, minGradient );
			updateGradient( 0, -1, redGradient( x, y, 0, -1 ), dx, dy, minGradient );
			updateGradient( 0, 1, redGradient( x, y, 0, 1 ), dx, dy, minGradient );
			static_assert(sizeof( getOriginalG( x + dx, y + dy ) * 3 ) == 4, "Have to be more than byte");
			setGreen( x, y, fitByte( (getOriginalG( x + dx, y + dy ) * 3 + getOriginalG( x - dx, y - dy ) + getOriginalR( x, y ) - getOriginalR( x + 2 * dx, y + 2 * dy )) / 4 ) );
		}
	}
}

void CBayerProcessor::greenByGradientMinBlue()
{
	for( int x = 3; x < width - 2; x += 2 ) {
		for( int y = 3; y < height - 2; y += 2 ) {
			int dx = 0;
			int dy = 0;
			__int16 minGradient = (1 << 15) - 1;
			updateGradient( -1, 0, blueGradient( x, y, -1, 0 ), dx, dy, minGradient );
			updateGradient( 1, 0, blueGradient( x, y, 1, 0 ), dx, dy, minGradient );
			updateGradient( 0, -1, blueGradient( x, y, 0, -1 ), dx, dy, minGradient );
			updateGradient( 0, 1, blueGradient( x, y, 0, 1 ), dx, dy, minGradient );
			setGreen( x, y, fitByte( (getOriginalG( x + dx, y + dy ) * 3 + getOriginalG( x - dx, y - dy ) + getOriginalB( x, y ) - getOriginalB( x + 2 * dx, y + 2 * dy )) / 4 ) );
		}
	}
}

void CBayerProcessor::fillLineByMean( int y )
{
	for( int x = 0; x < width; x++ ) {
		fillPixelByMean( x, y );
	}

}

void CBayerProcessor::fillColumnByMean( int x )
{
	for( int y = 0; y < height; y++ ) {
		fillPixelByMean( x, y );
	}
}

void CBayerProcessor::fillBlueRedOnOriginalGreen()
{
	for( int y = 1; y < height - 1; y++ ) {
		if( (y % 2) == 1 ) {
			for( int x = 2; x < width - 1; x += 2 ) {
				setBlue( x, y, hueTransit( getG( x - 1, y ), getOriginalG( x, y ), getG( x + 1, y ), getOriginalB( x - 1, y ), getOriginalB( x + 1, y ) ) );
				setRed( x, y, hueTransit( getG( x, y - 1 ), getOriginalG( x, y ), getG( x, y + 1 ), getOriginalR( x, y - 1 ), getOriginalR( x, y + 1 ) ) );
			}
		} else {
			for( int x = 1; x < width - 1; x += 2 ) {
				setBlue( x, y, hueTransit( getG( x, y - 1 ), getOriginalG( x, y ), getG( x, y + 1 ), getOriginalB( x, y - 1 ), getOriginalB( x, y + 1 ) ) );
				setRed( x, y, hueTransit( getG( x - 1, y ), getOriginalG( x, y ), getG( x + 1, y ), getOriginalR( x - 1, y ), getOriginalR( x + 1, y ) ) );
			}
		}
	}
}

inline __int16 CBayerProcessor::gradientBlueByRed( int x, int y, int dx, int dy )
{
	assert( abs( dx ) == 1 );
	assert( abs( dy ) == 1 );

	return absDiff( getOriginalB( x + dx, y + dy ), getOriginalB( x - dx, y - dy ) ) + absDiff( getOriginalR( x + 2 * dx, y + 2 * dy ), getOriginalR( x, y ) ) +
		absDiff( getOriginalR( x - 2 * dx, y - 2 * dy ), getOriginalR( x, y ) ) + absDiff( getG( x + dx, y + dy ), getG( x, y ) ) + absDiff( getG( x - dx, y - dy ), getG( x, y ) );
}

inline __int16 CBayerProcessor::gradientRedByBlue( int x, int y, int dx, int dy )
{
	assert( abs( dx ) == 1 );
	assert( abs( dy ) == 1 );

	return absDiff( getOriginalR( x + dx, y + dy ), getOriginalR( x - dx, y - dy ) ) + absDiff( getOriginalB( x + 2 * dx, y + 2 * dy ), getOriginalB( x, y ) ) +
		absDiff( getOriginalB( x - 2 * dx, y - 2 * dy ), getOriginalB( x, y ) ) + absDiff( getG( x + dx, y + dy ), getG( x, y ) ) + absDiff( getG( x - dx, y - dy ), getG( x, y ) );
}

inline unsigned int CBayerProcessor::hueTransitForBlue( int x, int y, int dx, int dy )
{
	return hueTransit( getG( x - dx, y - dy ), getG( x, y ), getG( x + dx, y + dy ), getOriginalB( x - dx, y - dy ), getOriginalB( x + dx, y + dx ) );
}

inline unsigned int CBayerProcessor::hueTransitForRed( int x, int y, int dx, int dy )
{
	return hueTransit( getG( x - dx, y - dy ), getG( x, y ), getG( x + dx, y + dy ), getOriginalR( x - dx, y - dy ), getOriginalR( x + dx, y + dx ) );
}

void CBayerProcessor::fillBlue()
{
	for( int x = 2; x < width - 2; x += 2 ) {
		for( int y = 2; y < height - 2; y += 2 ) {
			__int16 gradientNE = gradientBlueByRed( x, y, -1, 1 );
			__int16 gradientNW = gradientBlueByRed( x, y, 1, 1 );
			if( gradientNE < gradientNW ) {
				setBlue( x, y, hueTransitForBlue( x, y, -1, 1 ) );
			} else {
				setBlue( x, y, hueTransitForBlue( x, y, 1, 1 ) );
			}
		}
	}
}

void CBayerProcessor::fillRed()
{
	for( int x = 3; x < width - 2; x += 2 ) {
		for( int y = 3; y < height - 2; y += 2 ) {
			__int16 gradientNE = gradientRedByBlue( x, y, -1, 1 );
			__int16 gradientNW = gradientRedByBlue( x, y, 1, 1 );
			if( gradientNE < gradientNW ) {
				setRed( x, y, hueTransitForRed( x, y, -1, 1 ) );
			} else {
				setRed( x, y, hueTransitForRed( x, y, 1, 1 ) );
			}
		}
	}
}

void CBayerProcessor::PatternedPixelGrouping()
{
	// �������� ������� ������ (2 ����) �� �������� ��������. �������� �� ��� � ����������� ����� �������� �� ����� ����� ������
	for( int i = 0; i < 2; i++ ) {
		fillColumnByMean( i );
		fillLineByMean( i );
		fillColumnByMean( width - i - 1 );
		fillLineByMean( height - i - 1 );
	}

	// �������� ��� ������� ������� (��������� �� �������)
	greenByGradientMinRed();
	greenByGradientMinBlue();

	fillBlueRedOnOriginalGreen();
	fillBlue();
	fillRed();
}

void CBayerProcessor::ApplyChanges()
{
	const int bytesPerRow = image.Stride;
	const int bytesPerPixel = 3; // BGR24
	unsigned char* buffer = (unsigned char*)image.Scan0;
	int internalIndex = 0;
	int baseAdr = 0;
	for( int y = 0; y < height; y++ ) {
		int pixelAdr = baseAdr;
		for( int x = 0; x < width; x++ ) {
			assert( internalIndex == getIndex( x, y ) );

			register unsigned char b = buffer[pixelAdr] = blue[internalIndex];
			register unsigned char g = buffer[pixelAdr + 1] = green[internalIndex];
			register unsigned char r = buffer[pixelAdr + 2] = red[internalIndex];

			pixelAdr += bytesPerPixel;
			++internalIndex;
		}
		baseAdr += bytesPerRow;
	}
}

static const int LumaRed = 9798;	//	0.299
static const int LumaGreen = 19235;	//	0.587
static const int LumaBlue = 3735;	//	0.114
static const int CoeffNormalizationBitsCount = 15;
static const int CoeffNormalization = 1 << CoeffNormalizationBitsCount;

double CBayerProcessor::CalculatePSNR( Gdiplus::BitmapData& reference )
{
	const int width = reference.Width;
	const int height = reference.Height;
	assert( this->width == width );
	assert( this->height == height );
	const int bytesPerLine = reference.Stride;
	const int bytesPerPixel = 3; // BGR24
	BYTE *buffer = (BYTE *)reference.Scan0;

	double mse = 0;

	int baseAdr = 0;
	for( int y = 0; y < height; y++ ) {
		int pixelAdr = baseAdr;
		for( int x = 0; x < width; x++ ) {
			int B = buffer[pixelAdr]; // blue
			int G = buffer[pixelAdr + 1]; // green
			int R = buffer[pixelAdr + 2]; // red

			int referenceY = (LumaRed * R + LumaGreen * G + LumaBlue * B + (CoeffNormalization >> 1)) >> CoeffNormalizationBitsCount; // luminance

			int actualY = (LumaRed * getR( x, y ) + LumaGreen * getG( x, y ) + LumaBlue * getB( x, y ) + (CoeffNormalization >> 1)) >> CoeffNormalizationBitsCount; // luminance

			mse += (actualY - referenceY) * (actualY - referenceY);

			pixelAdr += bytesPerPixel;
		}
		baseAdr += bytesPerLine;
	}

	mse /= width * height;

	return 10 * log10( maxByte * maxByte / mse );
}

