#include <windows.h>
#include <gdiplus.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <tchar.h>
#include <time.h>
#include <cassert>

#include "BayerProcessor.h"

using namespace Gdiplus;

static const int CoeffNormalizationBitsCount = 15;
static const int CoeffNormalization = 1 << CoeffNormalizationBitsCount;

void process( BitmapData& pData, BitmapData& reference )
{
	time_t startAll = clock();
	CBayerProcessor processor( pData );

	time_t startPPG = clock();
	processor.PatternedPixelGrouping();
	time_t endPPG = clock();

	processor.ApplyChanges();

	time_t endAll = clock();
	_tprintf( _T( "Time all: %.3f\n" ), static_cast<double>(endAll - startAll) / CLOCKS_PER_SEC );
	_tprintf( _T( "Time PPG: %.3f\n" ), static_cast<double>(endPPG - startPPG) / CLOCKS_PER_SEC );
	_tprintf( _T( "MSE: %f\n" ), processor.CalculatePSNR( reference ) );
}

int GetEncoderClsid( const WCHAR* format, CLSID* pClsid )
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	ImageCodecInfo* pImageCodecInfo = NULL;

	GetImageEncodersSize( &num, &size );
	if( size == 0 )
		return -1;  // Failure

	pImageCodecInfo = (ImageCodecInfo*)(malloc( size ));
	if( pImageCodecInfo == NULL )
		return -1;  // Failure

	GetImageEncoders( num, size, pImageCodecInfo );

	for( UINT j = 0; j < num; j++ ) {
		if( wcscmp( pImageCodecInfo[j].MimeType, format ) == 0 ) {
			*pClsid = pImageCodecInfo[j].Clsid;
			free( pImageCodecInfo );
			return j;  // Success
		}
	}

	free( pImageCodecInfo );
	return -1;  // Failure
}

int _tmain( int argc, _TCHAR* argv[] )
{
	if( argc != 4 ) {
		_tprintf( _T( "Usage: BayerPattern <inputFile.bmp> <outputFile.bmp> <reference.bmp>\n" ) );
		return 0;
	}

	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR			gdiplusToken;
	GdiplusStartup( &gdiplusToken, &gdiplusStartupInput, NULL );

	{
		Bitmap GDIBitmap( argv[1] );
		Bitmap reference( argv[3] );

		int w = GDIBitmap.GetWidth();
		int h = GDIBitmap.GetHeight();
		assert( w == reference.GetWidth() );
		assert( h == reference.GetHeight() );

		BitmapData bmpData;
		Rect rc( 0, 0, w, h ); // whole image
		if( Ok != GDIBitmap.LockBits( &rc, ImageLockModeRead | ImageLockModeWrite, PixelFormat24bppRGB, &bmpData ) )
			_tprintf( _T( "Failed to lock image: %s\n" ), argv[1] );
		else
			_tprintf( _T( "File: %s\n" ), argv[1] );

		BitmapData referenceBmp;
		if( Ok != reference.LockBits( &rc, ImageLockModeRead, PixelFormat24bppRGB, &referenceBmp ) )
			_tprintf( _T( "Failed to lock image: %s\n" ), argv[3] );
		else
			_tprintf( _T( "Reference file: %s\n" ), argv[3] );

		process( bmpData, referenceBmp );

		GDIBitmap.UnlockBits( &bmpData );

		// Save result
		CLSID clsId;
		GetEncoderClsid( _T( "image/bmp" ), &clsId );
		GDIBitmap.Save( argv[2], &clsId, NULL );
	}

	GdiplusShutdown( gdiplusToken );

	return 0;
}