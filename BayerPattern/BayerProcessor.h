#pragma once
#include <windows.h>
#include <gdiplus.h>
#include <vector>

class CBayerProcessor
{
public:
	CBayerProcessor( Gdiplus::BitmapData& );
	~CBayerProcessor();

	// ���������������� ������ ������� Patterned Pixel Grouping
	void PatternedPixelGrouping();
	// �������� ����������� � Gdiplus::BitmapData
	void ApplyChanges();
	// ���������� ������� PSNR
	double CalculatePSNR( Gdiplus::BitmapData& );

private:
	Gdiplus::BitmapData& image;

	const int width;
	const int height;

	std::vector<unsigned char> red;
	std::vector<unsigned char> green;
	std::vector<unsigned char> blue;

	inline int getIndex( int x, int y ) const;

	inline unsigned char getOriginalR( int x, int y ) const;
	inline unsigned char getOriginalG( int x, int y ) const;
	inline unsigned char getOriginalB( int x, int y ) const;

	inline unsigned char getR( int x, int y ) const;
	inline unsigned char getG( int x, int y ) const;
	inline unsigned char getB( int x, int y ) const;

	inline void setRed( int x, int y, unsigned char value );
	inline void setGreen( int x, int y, unsigned char value );
	inline void setBlue( int x, int y, unsigned char value );

	inline void addRed( int fromX, int fromY, __int16& value, int & counter );
	inline void addGreen( int fromX, int fromY, __int16& value, int & counter );
	inline void addBlue( int fromX, int fromY, __int16& value, int & counter );

	inline __int16 redGradient( int x, int y, int dx, int dy );
	inline __int16 blueGradient( int x, int y, int dx, int dy );

	inline void fillPixelByMean( int x, int y );

	void greenByGradientMinRed();
	void greenByGradientMinBlue();

	void fillLineByMean( int y );
	void fillColumnByMean( int x );

	void fillBlueRedOnOriginalGreen();

	__int16 gradientBlueByRed( int x, int y, int dx, int dy );
	__int16 gradientRedByBlue( int x, int y, int dx, int dy );
	unsigned int hueTransitForRed( int x, int y, int dx, int dy );
	unsigned int hueTransitForBlue( int x, int y, int dx, int dy );
	void fillRed();
	void fillBlue();
};

//--------------------------------------------------------------------------------------------------------

inline int CBayerProcessor::getIndex( int x, int y ) const
{
	return y * width + x;
}

inline unsigned char CBayerProcessor::getOriginalR( int x, int y ) const
{
	assert( x % 2 == 0 && y % 2 == 0 );
	return red[getIndex( x, y )];
}

inline unsigned char CBayerProcessor::getOriginalG( int x, int y ) const
{
	assert( (x + y) % 2 == 1 );
	return green[getIndex( x, y )];
}

inline unsigned char CBayerProcessor::getOriginalB( int x, int y ) const
{
	assert( x % 2 == 1 && y % 2 == 1 );
	return blue[getIndex( x, y )];
}

inline unsigned char CBayerProcessor::getR( int x, int y ) const
{
	return red[getIndex( x, y )];
}

inline unsigned char CBayerProcessor::getG( int x, int y ) const
{
	return green[getIndex( x, y )];
}

inline unsigned char CBayerProcessor::getB( int x, int y ) const
{
	return blue[getIndex( x, y )];
}

inline void CBayerProcessor::setRed( int x, int y, unsigned char value )
{
	assert( !(x % 2 == 0 && y % 2 == 0) );
	red[getIndex( x, y )] = value;
}

inline void CBayerProcessor::setGreen( int x, int y, unsigned char value )
{
	assert( (x + y) % 2 == 0 );
	green[getIndex( x, y )] = value;
}

inline void CBayerProcessor::setBlue( int x, int y, unsigned char value )
{
	assert( !(x % 2 == 1 && y % 2 == 1) );
	blue[getIndex( x, y )] = value;
}

inline void CBayerProcessor::addRed( int fromX, int fromY, __int16& value, int & counter )
{
	if( fromX < 0 || fromX >= width || fromY < 0 || fromY >= height ) {
		return;
	}
	value += getOriginalR( fromX, fromY );
	++counter;
}

inline void CBayerProcessor::addGreen( int fromX, int fromY, __int16& value, int & counter )
{
	if( fromX < 0 || fromX >= width || fromY < 0 || fromY >= height ) {
		return;
	}
	value += getOriginalG( fromX, fromY );
	++counter;
}

inline void CBayerProcessor::addBlue( int fromX, int fromY, __int16& value, int & counter )
{
	if( fromX < 0 || fromX >= width || fromY < 0 || fromY >= height ) {
		return;
	}
	value += getOriginalB( fromX, fromY );
	++counter;
}

inline void CBayerProcessor::fillPixelByMean( int x, int y )
{
	if( (x + y) % 2 == 0 ) {
		// R || B, no green
		__int16 greenValue = 0;
		int count = 0;
		addGreen( x + 1, y, greenValue, count );
		addGreen( x, y + 1, greenValue, count );
		addGreen( x - 1, y, greenValue, count );
		addGreen( x, y - 1, greenValue, count );
		assert( count > 0 );
		setGreen( x, y, greenValue / count );
	} else {
		// G. �������� R � B �������� �� ��������� ��� �����������
		int dxR, dyR, dxB, dyB;
		if( x % 2 ) {
			dxR = dyB = 1;
			dxB = dyR = 0;
		} else {
			dxR = dyB = 0;
			dxB = dyR = 1;
		}
		__int16 blueValue = 0;
		int countBlue = 0;
		addBlue( x + dxB, y + dyB, blueValue, countBlue );
		addBlue( x - dxB, y - dyB, blueValue, countBlue );
		assert( countBlue > 0 );
		setBlue( x, y, blueValue / countBlue );
		// Red
		__int16 redValue = 0;
		int countRed = 0;
		addRed( x + dxR, y + dyR, redValue, countRed );
		addRed( x - dxR, y - dyR, redValue, countRed );
		assert( countRed > 0 );
		setRed( x, y, redValue / countRed );
	}
	if( x % 2 == 1 && y % 2 == 1 ) {
		// Blue, no red
		__int16 redValue = 0;
		int countRed = 0;
		addRed( x - 1, y - 1, redValue, countRed );
		addRed( x - 1, y + 1, redValue, countRed );
		addRed( x + 1, y - 1, redValue, countRed );
		addRed( x + 1, y + 1, redValue, countRed );
		assert( countRed > 0 );
		setRed( x, y, redValue / countRed );

	}
	if( x % 2 == 0 && y % 2 == 0 ) {
		// Red, no blue
		__int16 blueValue = 0;
		int countBlue = 0;
		addBlue( x - 1, y - 1, blueValue, countBlue );
		addBlue( x - 1, y + 1, blueValue, countBlue );
		addBlue( x + 1, y - 1, blueValue, countBlue );
		addBlue( x + 1, y + 1, blueValue, countBlue );
		assert( countBlue > 0 );
		setBlue( x, y, blueValue / countBlue );
	}
}
